cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cordova-plugin-rfduino.rfduino",
    "file": "plugins/cordova-plugin-rfduino/www/rfduino.js",
    "pluginId": "cordova-plugin-rfduino",
    "clobbers": [
      "rfduino"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-whitelist": "1.3.2",
  "cordova-plugin-rfduino": "0.1.4",
  "cordova-plugin-crosswalk-webview": "2.3.0"
};
// BOTTOM OF METADATA
});